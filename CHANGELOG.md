# Changelog

## Unreleased

* Add `is_int` parameter to HeatMapper that, if True, formats values as integers
    and not floating point numbers
