import argparse
import csv
from dataclasses import dataclass
from typing import Optional
import sys


LINE_END = " \\\\ \n"

TABLEHEADER = r"\begin{table}"
TABULARHEADER = r"\begin{tabular}"


FOOTER = r"""
\end{tabular}
\end{table}
"""


DATA = list[list[float]]


@dataclass
class RGB:
    r: int
    g: int
    b: int


    def latex(self) -> str:
        return "{" + ",".join(str(v) for v in [self.r, self.g, self.b]) + "}"


class HeatMapper:
    def __init__(self, data: DATA, largest: float, smallest: float,
                 is_int = False):
        self._data = data
        self.largest = largest
        self.smallest = smallest
        self.is_int = is_int
        self.low_colour = RGB(0xd7, 0x30, 0x1f)
        self.high_colour = RGB(0xfe, 0xf0, 0xd9)
        self.line_end: str = LINE_END
        self.row_header: list[str] = []
        self.row_footer: list[str] = []
        self.col_header: list[str] = []
        self.output = sys.stdout

    @staticmethod
    def read_data(datafile: str) -> 'HeatMapper':
        largest: Optional[float] = None
        smallest: Optional[float] = None
        data: DATA = []
        with open(datafile, "r", newline="") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                data_row: list[float] = []
                for cell in row:
                    value = float(cell)
                    if not largest or not smallest:
                        largest = value
                        smallest = value
                    else:
                        largest = max(value, largest)
                        smallest = min(value, smallest)
                    data_row.append(value)
                data.append(data_row)
        if largest is None or smallest is None:
            raise Exception("No data read")
        return HeatMapper(data, largest, smallest)

    def print_header(self):
        self.output.write(TABLEHEADER + "\n")
        self.output.write(r"\definecolor{heatmapLow}{RGB}" + self.low_colour.latex() +"\n")
        self.output.write(r"\definecolor{heatmapHigh}{RGB}" + self.high_colour.latex() +"\n")
        self.output.write(TABULARHEADER)
        cols = len(self._data[0]) + 1 if self.col_header else 0
        self.output.write(r"{" + "l" + "c" * cols + r"}" + "\n")

    def print_footer(self):
        self.output.write(FOOTER)

    def mk_colour(self, value: float) -> str:
        distance = (value - self.smallest) / (self.largest - self.smallest)
        return f"heatmapLow!{int(100*distance)}!heatmapHigh"

    def mk_value(self, value: float) -> str:
        if self.is_int:
            return str(int(value))
        return str(value)

    def mk_cell(self, value: float) -> str:
        return r"\cellcolor{" + self.mk_colour(value) + r"} " + self.mk_value(value)

    def print_data(self):
        if self.row_header:
            self.output.write("& " + " & ".join(self.row_header) + self.line_end)
        for index, row in enumerate(self._data):
            if self.col_header:
                self.output.write(self.col_header[index] + " & ")
            self.output.write(" & ".join(self.mk_cell(value) for value in row))
            self.output.write(self.line_end)
        if self.row_footer:
            self.output.write("& " + " & ".join(self.row_footer) + self.line_end)

    def print_all(self):
        self.print_header()
        self.print_data()
        self.print_footer()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data-file")
    args = parser.parse_args()
    mapper = HeatMapper(args.data_file)
    mapper.row_footer = ["CF", "HCF", "PIEF"]
    mapper.row_header = ["CF", "HCF", "PIEF"]
    mapper.col_header = ["CF-CHAIN", "HCF-CHAIN", "PIEF-CHAIN"]
    mapper.print_all()


if __name__ == "__main__":
    main()
